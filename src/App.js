import React from 'react';
import { Route, Router } from 'react-router';
import Layout from './components/Layout';
import Section from './components/Section';
import Contact from './components/Contact'

function App() {
  return (
    <Layout>
     <Section />
     <Router>
     <Route exact path='/contact' component={Contact} />
     </Router>
    </Layout>
  );
}

export default App;
