import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Section from './Section';
function Layout(props) {
  console.log(props);
  return (
    <div id="wrapper">
    <Header />
    {props.children} 
    <Footer />
    <div className="dmtop">Scroll to Top</div>
    
  </div>
  );
}

export default Layout;
